# Books

## Programming
- ■□□ [Clean Code](https://bitbucket.org/motoxa/knizhechki/src/master/books/pr/Clean_Code.pdf) [in progress]
- ■□□ Грокаем алгоритмы [in progress]
- □□□ [PHPUnit Manual](https://bitbucket.org/motoxa/knizhechki/src/master/books/pr/PHPUnit.pdf) [planned]
- □□□ [Pro Git](https://bitbucket.org/motoxa/knizhechki/src/master/books/pr/Pro_Git.pdf) [planned]
- □□□ [The Definitive Guide To MongoDB](https://bitbucket.org/motoxa/knizhechki/src/master/books/pr/MongoDB.pdf) [planned]
- □□□ [Learning MySQL](https://bitbucket.org/motoxa/knizhechki/src/master/books/pr/Learning_MySQL.pdf) [planned]
- □□□ [PostgreSQL for beginners](https://bitbucket.org/motoxa/knizhechki/src/master/books/pr/PostgreSQL.pdf) [planned]
- □□□ Full-Stack Vue.js 2 and Laravel 5 [planned]
- □□□ Vue.js Up and Running [planned]
- □□□ [The Road To Learn React](https://bitbucket.org/motoxa/knizhechki/src/master/books/pr/React.pdf) [planned]
- □□□ Code Complete [planned]
- □□□ Designing Data-Intensive Applications [planned]
- □□□ A Philosophy of Software Design [planned]
- □□□ Software Design X-Rays [planned]
- □□□ Developer Hegemony [planned]
- □□□ DDoS Handbook [planned]
- □□□ Build APIs You Won't Hate [planned]
- ■■■ Паттерны проектирования [done]
- ■■■ Приемы ООП. Паттерны проектирования [done]
- ■■■ Программист-прагматик. Путь от подмастерья к мастеру [done]
- ■■■ [Laravel. Up and Running](https://bitbucket.org/motoxa/knizhechki/src/master/books/pr/Laravel.pdf) [done]
- ■■■ [Eloquent Javascript](https://eloquentjavascript.net/) [done]

## Management
- ■■□ Getting Things Done [in progress]
- ■□□ [Канбан. Альтернативный путь в Agile](http://flibusta.site/b/479765/read) [in progress]
- ■□□ Сила воли. Как развить и укрепить [in progress]
- □□□ 4-Hour Workweek [planned]
- □□□ Total Money Makeover [planned]
- ■■□ Rework. Бизнес без предрассудков [in progress]

## Math, Stats and NN
- □□□ An Introduction to Statistical Learning [planned]
- □□□ Hierarchical Attention Networks for Document Classification [planned]
- □□□ Decision-Based Specification and Comparsion of Table Recognition Algorithms [planned]
- □□□ Feature Selection for Classification: A Review [planned]
- □□□ Extending Page Segmentation Algorithms for Mixed-Layout Document Processing [planned]
- ■■■ Deep Learning with Python [done]

---